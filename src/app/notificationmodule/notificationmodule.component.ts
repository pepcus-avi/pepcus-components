import { Component, OnInit } from '@angular/core';
import { Notification } from './notification.model';

@Component({
  selector: 'app-notificationmodule',
  templateUrl: './notificationmodule.component.html',
  styleUrls: ['./notificationmodule.component.css']
})
export class NotificationmoduleComponent implements OnInit {
  markAllRead = false;
  read = false;
  notifications: Notification[] = [
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false),
    new Notification('Lorem Ipsum is simply dummy',
      'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry',
      '../../assets/images/sonu.jpg', '30 mins ago', false)
  ];
  constructor() { }
  onMarkAllRead() {
    console.log('markAllRead====', this.markAllRead);
    this.markAllRead = true;
    this.read = true;
  }
  onClickRead(notification) {
    notification.read = true;
    console.log('notification=====', notification);
  }
  ngOnInit() {
  }

}
