export class Notification {
  public title: string;
  public description: string;
  public imagePath: string;
  public time: string;
  public read: boolean;

  constructor(name: string, desc: string, imagePath: string, time: string, read: boolean) {
    this.title = name;
    this.description = desc;
    this.imagePath = imagePath;
    this.time = time;
    this.read = read;
  }
}
