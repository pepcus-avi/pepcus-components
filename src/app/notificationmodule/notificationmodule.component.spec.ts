import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationmoduleComponent } from './notificationmodule.component';

describe('NotificationmoduleComponent', () => {
  let component: NotificationmoduleComponent;
  let fixture: ComponentFixture<NotificationmoduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationmoduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationmoduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
