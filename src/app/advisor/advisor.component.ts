import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-advisor',
  templateUrl: './advisor.component.html',
  styleUrls: ['./advisor.component.css']
})
export class AdvisorComponent implements OnInit {
  results: string[];
  constructor(private http: HttpClient) {
    console.log('hi----------');
  }

  ngOnInit(): void {
    // Make the HTTP request:
    console.log('hi======');
    this.http.get('./files/data.json').subscribe(data => {
      // Read the result field from the JSON response.
      this.results = data['results'];
      console.log('results========', this.results);
    });
  }

}
