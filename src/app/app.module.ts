import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';

import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { ServersComponent } from './servers/servers.component';
import { TooltipComponent } from './tooltip/tooltip.component';
import { AdvisorComponent } from './advisor/advisor.component';
import { AnimateComponent } from './animate/animate.component';
import { ChatComponent } from './chat/chat.component';
import { CropComponent } from './crop/crop.component';
import { NotificationComponent } from './notification/notification.component';
import { SocialmediaComponent } from './socialmedia/socialmedia.component';
import { TypographyComponent } from './typography/typography.component';
import { EmailtemplateComponent } from './emailtemplate/emailtemplate.component';
import { NotificationmoduleComponent } from './notificationmodule/notificationmodule.component';
import { NotificationlistComponent } from './notificationmodule/notificationlist/notificationlist.component';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent,
    ServersComponent,
    TooltipComponent,
    AdvisorComponent,
    AnimateComponent,
    ChatComponent,
    CropComponent,
    NotificationComponent,
    SocialmediaComponent,
    TypographyComponent,
    EmailtemplateComponent,
    NotificationmoduleComponent,
    NotificationlistComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
